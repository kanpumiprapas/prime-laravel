<?php

use Illuminate\Database\Seeder;

class FeedbackQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Cashier TH
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการด้วยความสุภาพนุ่มนวลยิ้มแย้มแจ่มใส มีความพร้อมในการให้บริการ และใส่ใจในการให้บริการ​',
            'answer' => '{ "0" : "ไม่พอใจ", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ทวนสอบชื่อนามสกุล ก่อนการชำระเงินทุกครั้ง',
            'answer' => '{ "1" : "ใช่", "0" : "ไม่ใช่" }',
            'language_id' => '1',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่สามารถตอบข้อสงสัยชี้แจงค่าใช้จ่ายแก่ท่านได้ อย่างถูกต้อง ชัดเจน และสุภาพ',
            'answer' => '{ "0" : "ไม่พอใจ", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการเป็นไปด้วยความรวดเร็ว เสมอภาคโดยไม่เลือกให้บริการแก่ผู้ใดเป็นพิเศษ',
            'answer' => '{ "0" : "ไม่พอใจ", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​ข้อเสนอแนะ/ข้อคิดเห็นอื่นๆ​',
            'answer' => '{}',
            'language_id' => '1',
            'department_id' => '1',
        ]);

        // Cashier EN
        DB::table('feedback_question')->insert([
            'question' => 'The staff is gentle and friendly staff, ​attentive and caring.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff verifies the name - surname before clarifying you the invoice.​',
            'answer' => '{ "1" : "Yes", "0" : "No" }',
            'language_id' => '2',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff can answer you the questions and clarify you the expenses with accuracy, clarity and polite​.​',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff provides with the fast speed. Equal by not choosing to provide special services to anyone.​',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'Suggestions / Other comments',
            'answer' => '{}',
            'language_id' => '2',
            'department_id' => '1',
        ]);

        // Cashier ZN
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否用心、细心的为您提供服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '收费员是否在每次缴费之前核对您的姓名',
            'answer' => '{ "1" : "有", "0" : "没有" }',
            'language_id' => '3',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否能准确、清晰、礼貌给您建议或回答问题',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员能够快速的为您提供服务，没有选择性服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '1',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '建议/其他意见',
            'answer' => '{}',
            'language_id' => '3',
            'department_id' => '1',
        ]);

        // OPD TH
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการด้วยความสุภาพนุ่มนวลยิ้มแย้มแจ่มใส มีความพร้อมในการให้บริการ และใส่ใจในการให้บริการ​',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ทวนสอบชื่อนามสกุล ก่อนทำหัตถการทุกครั้ง​',
            'answer' => '{ "1" : "ใช่", "0" : "ไม่ใช่" }',
            'language_id' => '1',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ให้คำแนะนำและตอบข้อสงสัยของท่าน ด้วยความถูกต้อง ชัดเจน และสุภาพ',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ให้บริการเป็นไปด้วยความรวดเร็ว เสมอภาคโดยไม่เลือกให้บริการแก่ผู้ใดเป็นพิเศษ​​',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​ข้อเสนอแนะ/ข้อคิดเห็นอื่นๆ​',
            'answer' => '{}',
            'language_id' => '1',
            'department_id' => '2',
        ]);

        // OPD EN
        DB::table('feedback_question')->insert([
            'question' => 'The staff is gentle and friendly staff, ​attentive and caring.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff verifies the name - surname before giving you the treatment.​​',
            'answer' => '{ "1" : "Yes", "0" : "No" }',
            'language_id' => '2',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff will advise and answer your questions. With accuracy, clarity and polite​.​',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff provides with the fast speed. Equal by not choosing to provide special services to anyone.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'Suggestions / Other comments',
            'answer' => '{}',
            'language_id' => '2',
            'department_id' => '2',
        ]);

        // OPD ZN
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否用心、细心的为您提供服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否会在每一次治疗之前询问您的姓名',
            'answer' => '{ "1" : "有", "0" : "没有" }',
            'language_id' => '3',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否能准确、清晰、礼貌给您建议或回答问题',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员能够快速的为您提供服务，没有选择性服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '2',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '建议/其他意见',
            'answer' => '{}',
            'language_id' => '3',
            'department_id' => '2',
        ]);

        // OR TH
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ให้บริการด้วยความสุภาพนุ่มนวลยิ้มแย้มแจ่มใส มีความพร้อมในการให้บริการ และใส่ใจในการให้บริการ​',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ทวนสอบชื่อนามสกุล ก่อนทำหัตถการทุกครั้ง',
            'answer' => '{ "1" : "ใช่", "0" : "ไม่ใช่" }',
            'language_id' => '1',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'ความสะอาดของสถานที่ อุปกรณ์ เตียง และเครื่องมือที่ใช้ในห้องผ่าตัด',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการเป็นไปด้วยความรวดเร็ว เสมอภาคโดยไม่เลือกให้บริการแก่ผู้ใดเป็นพิเศษ​​​​',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​ข้อเสนอแนะ/ข้อคิดเห็นอื่นๆ​',
            'answer' => '{}',
            'language_id' => '1',
            'department_id' => '3',
        ]);

        // OR EN
        DB::table('feedback_question')->insert([
            'question' => 'The staff is gentle and friendly staff, ​attentive and caring.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff verifies the name - surname before giving you the treatment.​​',
            'answer' => '{ "1" : "Yes", "0" : "No" }',
            'language_id' => '2',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'Cleanliness of the place, equipment, beds and tools used in the operating room.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff provides with the fast speed. Equal by not choosing to provide special services to anyone.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'Suggestions / Other comments',
            'answer' => '{}',
            'language_id' => '2',
            'department_id' => '3',
        ]);

        // OR ZN
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否用心、细心的为您提供服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否会在每一次治疗之前询问您的姓名',
            'answer' => '{ "1" : "有", "0" : "没有" }',
            'language_id' => '3',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '手术室、医疗器械、床是否干净',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员能够快速的为您提供服务，没有选择性服务',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '3',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '建议/其他意见',
            'answer' => '{}',
            'language_id' => '3',
            'department_id' => '3',
        ]);

        // Pharmacy TH
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการด้วยความสุภาพนุ่มนวลยิ้มแย้มแจ่มใส มีความพร้อมในการให้บริการ และใส่ใจในการให้บริการ​',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ทวนสอบชื่อนามสกุล การแพ้ยาของผู้รับบริการทุกครั้งก่อนการจ่ายยา​',
            'answer' => '{ "1" : "ใช่", "0" : "ไม่ใช่" }',
            'language_id' => '1',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'เจ้าหน้าที่ให้คำแนะนำและอธิบายการใช้ยา ด้วยความถูกต้อง ชัดเจน และสุภาพ',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่เปิดโอกาสให้ผู้รับบริการซักถามข้อสงสัยเกี่ยวกับการใช้ยา',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '​เจ้าหน้าที่ให้บริการเป็นไปด้วยความรวดเร็ว เสมอภาคโดยไม่เลือกให้บริการแก่ผู้ใดเป็นพิเศษ',
            'answer' => '{ "0" : "ไม่พอใจมาก", "1" : "พอใจมาก" }',
            'language_id' => '1',
            'department_id' => '4',
        ]);
        
        // Pharmacy EN
        DB::table('feedback_question')->insert([
            'question' => 'The staff is gentle and friendly staff, ​attentive and caring.​',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff verifies the name - surname , drug allergy of the patients before dispensing.​',
            'answer' => '{ "1" : "Yes", "0" : "No" }',
            'language_id' => '2',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff give the advice and explain how to use the medecine with accuracy, clarity and polite.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff gives the patients the opportunity to ask questions about drug use.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => 'The staff provides with the fast speed. Equal by not choosing to provide special services to anyone​.',
            'answer' => '{ "0" : "Extremely Poor", "1" : "Extremely Good" }',
            'language_id' => '2',
            'department_id' => '4',
        ]);

        // Pharmacy ZN
        DB::table('feedback_question')->insert([
            'question' => '工作人员是否用心、细心的为您提供服务​​​',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员在配药前核实患者的姓名和药物过敏情况​​',
            'answer' => '{ "1" : "有", "0" : "没有" }',
            'language_id' => '3',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员准确、清晰的解释如何使用药物 ',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员会让患者询问关于药物的使用问题',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '4',
        ]);
        DB::table('feedback_question')->insert([
            'question' => '工作人员能够快速的为您提供服务，没有选择性服务 意',
            'answer' => '{ "0" : "不满意", "1" : "非常满意" }',
            'language_id' => '3',
            'department_id' => '4',
        ]);
    }
}
