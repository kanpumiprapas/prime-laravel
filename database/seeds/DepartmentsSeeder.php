<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'cashier',
            'language' => '2'
        ]);
        DB::table('departments')->insert([
            'name' => 'opd',
            'language' => '2'
        ]);
        DB::table('departments')->insert([
            'name' => 'or',
            'language' => '2'
        ]);
        DB::table('departments')->insert([
            'name' => 'pharmcy',
            'language' => '2'
        ]);
    }
}
