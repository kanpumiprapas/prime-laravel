<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('language')->insert([
            'name' => 'thai',
            'short_name' => 'th',
        ]);
        DB::table('language')->insert([
            'name' => 'english',
            'short_name' => 'en',
        ]);
        DB::table('language')->insert([
            'name' => 'chinese',
            'short_name' => 'zn',
        ]);
    }
}
