<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Appointments;
use Illuminate\Support\Facades\Auth;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $appointment = new Appointments;
        $appointment->patient_id            = $request->patient_id;
        $appointment->hospital_num          = $request->hospital_num;
        $appointment->doctor_id             = $request->doctor_id;
        $appointment->appointment_date      = $request->appointment_date;
        $appointment->appointment_time      = $request->appointment_time;
        $appointment->appointment_detail    = $request->appointment_detail;
        $appointment->status                = $request->input('status', 1);
        $appointment->state                 = $request->input('state', 1);
        $appointment->created_date          = date("Y-m-d H:i:s");
        $appointment->created_by            = $user->id;
        
        if($appointment->save()) {
            $data['status'] = true;
            $data['message'] = 'Create Success';
            $this->Status = 200;
        } else {
            $data['status'] = true;
            $data['message'] = 'Can\'t Create';
            $this->Status = 500;
        }

        return response()->json($data, $this->Status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $appointment = Appointments::where('id', $id)
                        ->update([
                            'patient_id' => $request->patient_id,
                            'hospital_num' => $request->hospital_num,
                            'doctor_id' => $request->doctor_id,
                            'appointment_date' => $request->appointment_date,
                            'appointment_time' => $request->appointment_time,
                            'appointment_detail' => $request->appointment_detail,
                            'status' => $request->input('status', 1),
                            'state' => $request->input('state', 1)
                        ]);

        if($appointment) {
            $data['status'] = true;
            $data['message'] = 'Update Success';
            $this->Status = 200;
        } else {
            $data['status'] = true;
            $data['message'] = 'Can\'t Update';
            $this->Status = 500;
        }

        return response()->json($data, $this->Status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointments::where('id', $id)
                                        ->update(['state' => -2]);

        if($appointment) {
            $data['status'] = true;
            $data['message'] = 'Delete Success';
            $this->Status = 200;
        } else {
            $data['status'] = true;
            $data['message'] = 'Can\'t Delete';
            $this->Status = 500;
        }

        return response()->json($data, $this->Status);
    }

    public function attachment($id) 
    {
        $username = 'primeapi';
        $password = '6b2Cd%J(^aDl';
        $URL = 'http://110.49.49.226:8295/prime/v2/history';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        // TODO Post
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "HN=".$id);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result=curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close ($ch);
        $json = json_decode($result, true);
        return response()->json($json, $status_code);
    }
}
