<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Model\FeedbackQuestion;

class FeedbackQuestionController extends Controller
{
    public function index(Request $request)
    {
        $lang = $request->query('lang');
        $dept = $request->query('dept');
        
        if($lang && $dept) {
            $feedbackQuestion = FeedbackQuestion::whereHas('language', function (Builder $query) use ($lang) {
                $query->where('short_name', $lang);
            })->whereHas('department', function (Builder $query) use ($dept) {
                $query->where('name', $dept);
            })->get();
        } else if($lang) {
            $feedbackQuestion = FeedbackQuestion::whereHas('language', function (Builder $query) use ($lang) {
                $query->where('short_name', $lang);
            })->get();
        } else if($dept) {
            $feedbackQuestion = FeedbackQuestion::whereHas('department', function (Builder $query) use ($dept) {
                $query->where('name', $dept);
            })->get();
        } else {
            $feedbackQuestion = FeedbackQuestion::with(['language', 'department'])->get();
        }

        $items = [];
        foreach($feedbackQuestion as $key => $value) {
            $items[$key]['id']           = $value['id'];
            $items[$key]['question']     = $value['question'];
            $items[$key]['answer']       = json_decode($value['answer']);
            $items[$key]['language']     = $value['language']->name;
            $items[$key]['department']   = $value['department']->name;
        }

        return response()->json($items, 200);
    }
}
