<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\User;
use App\Model\Profile;
use Illuminate\Support\Facades\Auth;
use Validator;


class AuthController extends Controller
{
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $errors = $validator->errors();

        if ($errors->has('email')) {
            $data['status'] = true;
            $data['message'] = 'email wrong';
            $data['result'] = null;
        } else {
            $input['password'] = bcrypt($input['password']);
            $user = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => $input['password'],
                'c_password' => $input['password'],
            ]);
            if($user)
            {
                $profile = new Profile;
                $profile->user_id = $user->id;
                $profile->hospital_number = $request->hospital_number;
                $profile->title_name = $request->title_name;
                $profile->first_name = $request->first_name;
                $profile->last_name = $request->last_name;
                $profile->phone = $request->phone;
                $profile->birthdate = $request->birthdate;
                $profile->identity_type = $request->identity_type ? $request->identity_type : "citzen_id";
                $profile->identity_id = $request->identity_id;
                if($request->identity_image) {
                    $extensionIdentity = $request->identity_image->extension();
                    $path = $request->file('identity_image')->storeAs(
                        'identity_image',
                        $user->id.'.'.$extensionIdentity, 
                        'public'
                    );
                    $profile->identity_image = $path;
                }
                if($request->avatar) {
                    $extensionAvatar = $request->avatar->extension();
                    $pathAvatar = $request->file('avatar')->storeAs(
                        'avatar',
                        $user->id.'.'.$extensionAvatar, 
                        'public'
                    );
                    $profile->avatar = $pathAvatar;
                }
                $profile->save();
            }
            // $success['token'] = $user->createToken('AppName')->accessToken;
            $data['status'] = true;
            $data['message'] = 'register success';
            $data['result'] = null;
            $data['token'] = $user->createToken('AppName')->accessToken;
        }
        return response()->json($data, $this->successStatus);
    }

    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $profile = Profile::where('user_id', $user->id)->first();
            $data['status'] = true;
            $data['message'] = 'login success';
            $data['result'] = $user;
            if($profile) {
                $data['result']['profile'] = $profile;
                if($profile['identity_image']) {
                    $data['result']['profile']['identity_image'] = asset('storage/'.$profile['identity_image']);
                }
                if($profile['avatar']) {
                    $data['result']['profile']['avatar'] = asset('storage/'.$profile['avatar']);
                }
            }
            $data['token'] = $user->createToken('AppName')->accessToken;
            // $success['status'] = $this->successStatus;
            // $success['token'] =  $user->createToken('AppName')->accessToken;
            // $success['detail'] = $user;
            return response()->json($data, $this->successStatus);
        } else {
            $data['status'] = false;
            $data['message'] = 'Unauthorised';
            $data['result'] = null;
            return response()->json($data, 401);
        }
    }
}
