<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Profile;
use App\Model\User;

class UserController extends Controller
{
    public $successStatus = 200;

    public function getUser()
    {
        $id = Auth::id();
        $profile = User::find($id)->profile;
        $data['status'] = true;
        $data['message'] = 'detail user';
        $data['result']['user'] = User::find($id);
        $data['result']['profile'] = $profile;
        if($profile['identity_image']) {
            $data['result']['profile']['identity_image'] = asset('storage/'.$profile['identity_image']);
        }
        if($profile['avatar']) {
            $data['result']['profile']['avatar'] = asset('storage/'.$profile['avatar']);
        }

        return response()->json($data, $this->successStatus);
    }
}
