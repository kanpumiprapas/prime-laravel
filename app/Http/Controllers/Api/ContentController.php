<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public $successStatus = 200;
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $page = $request->input('page') ? "page=".$request->input('page') : "page=1";
        $per_page = $request->input('per_page') ? "&per_page=" .$request->input('per_page') : "&per_page=100";
        $categories = $request->input('categories') ? "&categories=".$request->input('categories') : "";
        $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/posts?{$page}{$per_page}{$categories}";
        $json = json_decode(file_get_contents($url), true);
        $data['status'] = true;
        $data['message'] = 'Content List';
        $i = 0;
        foreach ($json as $value) {
            $mediaLink = $value['_links']['wp:featuredmedia']['0']['href'];
            $media = json_decode(file_get_contents($mediaLink), true);
            $image = $media['media_details']['sizes'];
            $detail[$i]['id'] = $value['id'];
            $detail[$i]['title'] = $value['title']['rendered'];
            $detail[$i]['content'] = $value['content']['rendered'];
            $detail[$i]['categories'] = $value['categories'];
            $detail[$i]['date'] = $value['date'];
            $detail[$i]['thumbnail'] = $image?$image['thumbnail']['source_url']:'';
            $detail[$i]['full_image'] = $image?$image['full']['source_url']:'';
            $i++;
        }
        $data['result'] = $detail;

        return response()->json($data, $this->successStatus);
    }

    public function show($id)
    {
        $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/posts/" . $id;
        $json = json_decode(file_get_contents($url), true);
        $data['status'] = true;
        $data['message'] = 'Single Content';
        $detail['id'] = $json['id'];
        $detail['title'] = $json['title']['rendered'];
        $detail['content'] = $json['content']['rendered'];
        $detail['categories'] = $json['categories'];
        $detail['date'] = $json['date'];
        $data['result'] = $detail;

        return response()->json($data, $this->successStatus);
    }

    public function listService(Request $request)
    {
        $lang = $request->input('lang');
        switch($lang) {
            case "en":
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=3137";
            break;
            case "cn":
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=3208";
            break;
            default:
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=688";
        }
        
        $json = json_decode(file_get_contents($url), true);
        $data['status'] = true;
        $data['message'] = 'Service List';
        $i = 0;
        foreach ($json as $value) {
            $mediaLink = $value['_links']['wp:featuredmedia']['0']['href'];
            $media = json_decode(file_get_contents($mediaLink), true);
            $image = $media['media_details']['sizes'];
            $detail[$i]['id'] = $value['id'];
            $detail[$i]['title'] = $value['title']['rendered'];
            $detail[$i]['content'] = $value['content']['rendered'];
            $detail[$i]['date'] = $value['date'];
            $detail[$i]['thumbnail'] = $image?$image['thumbnail']['source_url']:'';
            $detail[$i]['full_image'] = $image?$image['full']['source_url']:'';
            $i++;
        }
        $data['result'] = $detail;

        return response()->json($data, $this->successStatus);
    }

    public function listPromotion(Request $request)
    {
        $lang = $request->input('lang');
        switch($lang) {
            case "en":
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=3137";
            break;
            case "cn":
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=3208";
            break;
            default:
                $url = "https://www.primefertilitycenter.com/wp-json/wp/v2/pages?parent=3177";
        }
        
        $json = json_decode(file_get_contents($url), true);
        $data['status'] = true;
        $data['message'] = 'Promotion List';
        $i = 0;
        foreach ($json as $value) {
            $mediaLink = $value['_links'];
            $featuredmedia = in_array('wp:featuredmedia', $mediaLink);
            // $mediaLink = $value['_links']['wp:featuredmedia']['0']['href'];
            if($featuredmedia) {
                $href = $mediaLink['wp:featuredmedia']['0']['href'];
                $media = json_decode(file_get_contents($href), true);
                $image = $media['media_details']['sizes'];
            } else {
                $image = null;
            }
            
            $detail[$i]['id'] = $value['id'];
            $detail[$i]['title'] = $value['title']['rendered'];
            $detail[$i]['content'] = $value['content']['rendered'];
            $detail[$i]['date'] = $value['date'];
            $detail[$i]['thumbnail'] = $image??$image['thumbnail']['source_url'];
            $detail[$i]['full_image'] = $image??$image['full']['source_url'];
            $i++;
        }
        $data['result'] = $detail;

        return response()->json($data, $this->successStatus);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Content $content)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Content $content)
    {
        //
    }
}
