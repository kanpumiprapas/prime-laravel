<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Doctor;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private static $urlPrime = 'https://app.primefertilitycenter.com/code/assets/engines/img/upload/doctor/';

    public function index(Request $request)
    {
        $doctors = Doctor::all();
        $lang = $request->input('lang');
        if($lang) {
            $doctors = Doctor::where('lang', $lang)->get();
        }
        
        $data['status'] = true;
        $data['message'] = 'all doctor';
        $data['result'] = null;

        
        foreach($doctors as $key => $item) {
            $doctor[$key]['id'] = $item->id;
            $doctor[$key]['doctor_num'] = $item->doctor_num;
            $doctor[$key]['doctor_name'] = $item->doctor_name;
            $doctor[$key]['doctor_img'] = self::$urlPrime.$item->doctor_img;
            $doctor[$key]['doctor_desc'] = $item->doctor_desc;
            $doctor[$key]['state'] = $item->state;
            $doctor[$key]['created_date'] = $item->created_date;
            $doctor[$key]['created_by'] = $item->created_by;
            $doctor[$key]['modified_date'] = $item->modified_date;
            $doctor[$key]['modifiled_by'] = $item->modifiled_by;
        }

        $data['result'] = $doctor;

        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id)
    {
        $doctor = Doctor::where('id', $id)->first();
        if($doctor) {
            $doctor->schedule;
        }
        $doctor->doctor_img = self::$urlPrime.$doctor->doctor_img;
        $data['status'] = true;
        $data['message'] = 'detail doctor';
        $data['result'] = $doctor;

        return response()->json($data, 200);
    }
}
