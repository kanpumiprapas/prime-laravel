<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Model\Video;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = 'th')
    {
        $url = 'https://'.$_SERVER['SERVER_NAME'].'/';
        if($lang) {
            $videos = Video::where('video_language', $lang)->get();
        } else {
            $videos = Video::all();
        }

        $data['status'] = true;
        $data['message'] = 'List Video';
        $video = 'Data No Found';

        if($videos) {
            foreach($videos as $key => $item) {
                $video[$key] = $item;
                $video[$key]['video_name'] = $url.$item['video_name'];
                $video[$key]['video_thumbnail'] = $url.$item['video_thumbnail'];
            }
        }


        $data['result'] = $video;

        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pathVideo = null;
        $pathVideoThumbnail = null;
        if($request->video_name) {
            $random = Str::random(40);
            $extensionVideo = $request->video_name->extension();
            $video_name = $request->file('video_name');
            $video_name_path = '../code/assets/engines/video/upload/'.$request->video_language;
            $video_name_name = $random.'.'.$extensionVideo;
            $video_name->move($video_name_path, $video_name_name);
            $pathVideo = 'code/assets/engines/video/upload/'.$request->video_language.'/'.$video_name_name;
        }
        if($request->video_thumbnail) {
            $random = Str::random(40);
            $extensionVideoThumbnail = $request->video_thumbnail->extension();
            $video_thumbnail = $request->file('video_thumbnail');
            $video_thumbnail_path = '../code/assets/engines/img/upload/video_thumbnail/'.$request->video_language;
            $video_thumbnail_name = $random.'.'.$extensionVideoThumbnail;
            $video_thumbnail->move($video_thumbnail_path, $video_thumbnail_name);
            $pathVideoThumbnail = 'code/assets/engines/img/upload/video_thumbnail/'.$request->video_language.'/'.$video_thumbnail_name;
        }
        $videos = Video::find($id);
        $videos->title = $request->title;
        $videos->video_name = $pathVideo;
        $videos->video_thumbnail = $pathVideoThumbnail;
        $videos->video_language = $request->video_language;
        $videos->status = $request->status;
        $videos->modified_date = date('Y-m-d H:i:s');
        // $videos->modified_by = $request->modified_by;
        $videos->save();

        $data['status'] = true;
        $data['message'] = 'Update Success';
        $data['result'] = $videos;

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
