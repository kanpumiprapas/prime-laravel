<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Doctor;

class AppointmentsHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private static $urlPrime = 'https://app.primefertilitycenter.com/code/assets/engines/img/upload/doctor/';

    public function index($id)
    {
        $username = 'primeapi';
        $password = '6b2Cd%J(^aDl';
        $URL = 'http://110.49.49.226:8295/prime/v2/appointment';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$URL);
        // TODO Post
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "HN=".$id);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        $result=curl_exec ($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        curl_close ($ch);
        $json = json_decode($result, true);
        return response()->json($json, $status_code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id) {
            $username = 'primeapi';
            $password = '6b2Cd%J(^aDl';
            $URL = 'http://110.49.49.226:8295/prime/v2/appointment';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "HN=".$id);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            $result=curl_exec ($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            curl_close ($ch);
            $json = json_decode($result, true);

            $doctorCode = $json['appointment_detail'][0]['data']['Doctor_Code'];
            $doctor = Doctor::where('doctor_code', $doctorCode)->get();
            $json['appointment_detail'][0]['data']['Doc_Name'] = $doctor[0]->doctor_name_th;
            $json['appointment_detail'][0]['data']['Doc_img'] = self::$urlPrime.$doctor[0]->doctor_img;

            $data['status'] = true;
            $data['message'] = 'Appointment';
            $status = $status_code;
            $data['result'] = $json;
        } else {
            $data['status'] = true;
            $data['message'] = 'Can\'t find appointmentt';
            $status = 500;
            $data['result'] = null;
        }

        return response()->json($data, $status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
