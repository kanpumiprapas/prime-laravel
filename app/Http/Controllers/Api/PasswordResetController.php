<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Model\User;
use App\Model\Profile;
use App\Model\PasswordReset;
use Hash;

class PasswordResetController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
             ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token)
            );
            $data['status'] = true;
            $data['message'] = 'We have e-mailed your password reset link!';
            $data['result'] = null;
        return response()->json($data, 200);
    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }

        $data['status'] = true;
        $data['message'] = 'reset password';
        $data['result'] = null;
        $data['token'] = $passwordReset->token;

        return response()->json($data, 200);
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();

        $data['status'] = true;

        if (!$passwordReset)
            $data['message'] = 'This password reset token is invalid.';
            return response()->json($data, 404);

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user)
            $data['message'] = 'We can\'t find a user with that e-mail address.';
            return response()->json($data, 404);

        $user->password = bcrypt($request->password);
        $user->save();
        if($user) {
            $profile = Profile::where('user_id', $user->id)->first();
            $data['message'] = 'reset success';
            $data['result']['user'] = $user;
            $data['result']['profile'] = $profile;
            $data['result']['profile']['identity_image'] = url('/'.$profile['identity_image']);
        }
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json($data, 200);
    }

    public function changePassword ( Request $request ) 
    {
        $user = Auth::user();
        if($user) {
            $data['status'] = true;
            $current_password = $request->input('current_password');

            $validator = Validator::make($request->all(), [
                'current_password' => 'required',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);
    
            if ($validator->fails()) {
                
                $errors = $validator->errors();
                if($errors->first('current_password')) {
                    $data['error']['current_password'] = $errors->first('current_password');
                }
                if($errors->first('password')) {
                    $data['error']['password'] = $errors->first('password');
                }
                if($errors->first('c_password')) {
                    $data['error']['c_password'] = $errors->first('c_password');
                }

                return response()->json($data, 200);
            }



            if(!Hash::check($current_password, $user->password)) {
                $data['error']['current_password'] = 'Current password is not match';
                return response()->json($data, 200);
            }

            $request->user()->fill([
                'password' => Hash::make($request->password)
            ])->save();
            $data['message'] = 'Change Password Sucess';
            return response()->json($data, 200);

        }
    }
}
