<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index($lang)
    {
        switch($lang) {
            case "en":
                $url = "https://primefertilitycenter.com/wp-json/wp/v2/pages/7226";
            break;
            case "zh":
                $url = "https://primefertilitycenter.com/wp-json/wp/v2/pages/7228";
            break;
            default:
                $url = "https://primefertilitycenter.com/wp-json/wp/v2/pages/7221";
        }
        
        $json = json_decode(file_get_contents($url), true);
        $data['status'] = true;
        $data['message'] = 'About Us';
        $content = $json['content']['rendered']; 
        $data['content'] = preg_replace('/\[\/?et_pb.*?\]/', '', $content);

        return response()->json($data, 200);
    }
}
