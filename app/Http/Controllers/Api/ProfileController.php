<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Model\Profile;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id = Auth::id();
        $profile = Profile::find($id);
        $data['status'] = true;
        $data['message'] = 'Profile';
        $data['result']['profile'] = $profile;
        if($profile['identity_image']) {
            $data['result']['profile']['identity_image'] = asset('storage/'.$profile['identity_image']);
        }
        if($profile['avatar']) {
            $data['result']['profile']['avatar'] = asset('storage/'.$profile['avatar']);
        }

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = Auth::id();
        $path = null;
        $pathAvatar = null;
        if($request->identity_image) {
            $extensionIdentity = $request->identity_image->extension();
            $path = $request->file('identity_image')->storeAs(
                'identity_image',
                $id.'.'.$extensionIdentity, 
                'public'
            );
        }
        if($request->avatar) {
            $extensionAvatar = $request->avatar->extension();
            $pathAvatar = $request->file('avatar')->storeAs(
                'avatar',
                $id.'.'.$extensionAvatar, 
                'public'
            );
        }
        $profile = Profile::find($id);
        $profile->hospital_number = $request->hospital_number;
        $profile->title_name = $request->title_name;
        $profile->first_name = $request->first_name;
        $profile->last_name = $request->last_name;
        $profile->avatar = $pathAvatar;
        $profile->phone = $request->phone;
        $profile->birthdate = $request->birthdate;
        $profile->identity_type = $request->identity_type;
        $profile->identity_id = $request->identity_id;
        $profile->identity_image = $path;
        $profile->save();

        $data['status'] = true;
        $data['message'] = 'Update Success';
        $data['result']['profile'] = $profile;

        return response()->json($data, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
