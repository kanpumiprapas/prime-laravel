<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'video';
    public $timestamps = false;
}
