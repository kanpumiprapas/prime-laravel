<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'language';
    public function feedbackquestion()
    {
        return $this->hasMany('App\Model\FeedbackQuestion');
    }
}
