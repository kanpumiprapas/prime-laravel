<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'appointment';
    public $timestamps = false;

    public function doctor()
    {
        return $this->belongsTo('App\Model\Doctor');
    }
}
