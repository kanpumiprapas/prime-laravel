<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DoctorSchedule extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'doctor_schedule';
    protected $primaryKey = 'id';

    public function doctor()
    {
        return $this->belongsTo('App\Model\Doctor');
    }
}
