<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FeedbackQuestion extends Model
{
    protected $table = 'feedback_question';

    public function department()
    {
        return $this->belongsTo('App\Model\Departments');
    }

    public function language()
    {
        return $this->belongsTo('App\Model\Language');
    }
}
