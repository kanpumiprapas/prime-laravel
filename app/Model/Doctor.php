<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'doctor';
    protected $primaryKey = 'id';

    public function schedule()
    {
        return $this->hasMany('App\Model\DoctorSchedule', 'doctor_id', 'id');
    }

    public function appointments()
    {
        return $this->hasMany('App\Model\Appointments', 'doctor_id', 'id');
    }
}
