<?php

use Illuminate\Http\Request;
use App\Http\Resources\Video as VideoResource;
use App\Model\Video;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('v1')->group(function(){
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('getUser', 'Api\UserController@getUser');
    });
    Route::prefix('password')->group(function(){
        Route::post('create', 'Api\PasswordResetController@create');
        Route::get('find/{token}', 'Api\PasswordResetController@find');
        Route::post('reset', 'Api\PasswordResetController@reset');
        Route::group(['middleware' => 'auth:api'], function(){
            Route::post('changepassword', 'Api\PasswordResetController@changePassword');
        });
    });
    Route::prefix('content')->group(function(){
        Route::get('list', 'Api\ContentController@list');
        Route::get('listservice', 'Api\ContentController@listService');
        Route::get('listpromotion', 'Api\ContentController@listPromotion');
        Route::get('{id}', 'Api\ContentController@show');
    });
    Route::prefix('aboutus')->group(function(){
        Route::get('/{lang}', 'Api\AboutUsController@index');
    });

    Route::prefix('appointments-history')->group(function(){
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('/{id}', 'Api\AppointmentsHistoryController@show');
        });
    });

    Route::prefix('appointments')->group(function(){
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('{id}', 'Api\AppointmentsController@index');
            Route::post('/', 'Api\AppointmentsController@store');
            Route::post('update/{id}', 'Api\AppointmentsController@update');
            Route::delete('{id}', 'Api\AppointmentsController@destroy');
            Route::get('attachment/{id}', 'Api\AppointmentsController@attachment');
        });
    });

    Route::prefix('doctor')->group(function(){
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('/', 'Api\DoctorController@index');
            Route::get('/{id}', 'Api\DoctorController@detail');
        });
    });

    Route::prefix('profile')->group(function(){
        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('/', 'Api\ProfileController@show');
            Route::post('update', 'Api\ProfileController@update');
        });
    });

    Route::apiResource('video/{lang?}', 'Api\VideoController');

    Route::prefix('feedback')->group(function(){
        Route::get('question', 'Api\FeedbackQuestionController@index');
        Route::group(['middleware' => 'auth:api'], function(){
        });
    });
});